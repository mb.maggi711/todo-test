
## INICIACIÓN DEL PROYECTO CON DOCKER-COMPOSE

Antes de comenzar, asegúrate de ajustar las variables en el archivo docker-compose.yml que corresponden a la configuración de MySQL:

 - todonj

Asegúrate de ejecutar todos los comandos en la raíz de la carpeta docker, donde se encuentra el archivo Makefile.

#### Iniciar Contenedora

Para iniciar la contenedora, ejecuta el siguiente comando:

```bash
  make -s build
```
Una vez que la contenedora esté en funcionamiento, abre tu navegador web y navega a:

```bash
  http://127.0.0.1
```

#### Apagar contenedora 

Para apagar la contedora ejecutar el siguiente comando:

```bash
  make -s down 
```

#### Levantar contenedora 

Si se ha dado el comando make -s down y es necesario levantar nuevamente las contenedoras usa el siguiente comando:

```bash
  make -s up 
```

#### Ingresar a la base de datos

Para poder ingresar la base de datos se ejecuta el siguiente comando 

```bash
  make -s mysql
```
#### Validar conexión con MySQL

Validar conexión con MySQL 

```bash
  make -s validate
```