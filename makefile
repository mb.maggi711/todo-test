.PHONY: k-ns
kns:
	@echo "Initializing namespace..."
	kubectl --kubeconfig=$(KUBECONF) apply -f namespace.yaml


.PHONY: kca
kca:
	@echo "The kubectl command targeting all namespaces..."
	kubectl --kubeconfig=$(KUBECONF) get ns


.PHONY: kaf-lb
kaf-lb:
	@echo "Apply a YML file for LB..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) apply -f service-lb.yaml 


.PHONY: kgs
kgs:
	@echo "List all services in ps output format..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) get svc 


.PHONY: kaf-pvc
kaf-pvc:
	@echo "Apply a YML file for mysql-pvc..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) apply -f mysql-pvc.yaml


.PHONY: kgpv
kgpv:
	@echo "List pvc services in ps output format..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) get pvc


.PHONY: kaf-my
kaf-my:
	@echo "Apply a YML file for MySQL..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) apply -f mysql-deployment.yaml 

.PHONY: kgp
kgp:
	@echo "List all pods in ps output format..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) get pods -o wide

.PHONY: kaf-de
kaf-de:
	@echo "Apply a YML file for the APP..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) apply -f deployment.yaml 

.PHONY: kgi
kgi:
	@echo "List external IP output format for the LB..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) get svc | grep -i 'loadbalancer' | awk '{print "La IP asignada al balanceador de carga es: " $4}'

.PHONY: kdelf
kdelf:
	@echo "Delete a pod using the type and name specified in -f argument..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) delete -f $(yaml)

.PHONY: kdelp
kdelf:
	@echo "Delete a pod using the type and name specified in -f argument..."
	kubectl --kubeconfig=$(KUBECONF) -n $(namespace) delete $(name)