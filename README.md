#Proyecto TodoList

Dentro del repositorio, se encuentran las siguientes carpetas:

	- docker
	- kubernetes
	- src
	- terraform
	
	
Cada una de estas carpetas contiene archivos esenciales para el despliegue tanto en entornos locales como en plataformas en la nube, en este caso, DigitalOcean.

En cada carpeta, se encuentran dos archivos importantes:

	- Makefile: Este archivo contiene las instrucciones para automatizar tareas comunes durante el desarrollo y despliegue del aplicativo.
	- Readme.md: Aquí se encuentran las instrucciones detalladas para desplegar la aplicación utilizando el archivo Makefile correspondiente a cada carpeta.


Estos archivos son fundamentales para asegurar un proceso de despliegue consistente y eficiente, tanto en entornos locales como en la nube de DigitalOcean.

#Imagenes

- Check creación cluster

![](./img/cluster.png)

- Check PVC

![](./img/pvc.png)

- Check IP LB

![](./img/ip_lb.png)

- Check website

![](./img/site.png)

- Prueba de funcionamiento

![](./img/prueba.png)

- check prueba en MySQL

![](./img/check_prueba.png)