
## INICIACIÓN DEL PROYECTO IAC

Deploy basico de infraestructura bajo código.

Puntos importantes a tener en cuenta:

- Usar ```bash export token=tokendeaccesoalproveedordecloud ```
- El comando tf-plan genera automanticamente el archivo tfplan.out
- Los comando tf-apply y tf-destroy tienen aplicada lo opción -auto-approve que no pide verificación para su ejecución
- Se genera automanticamente el archivo kubeconfig.yaml con el cual se podra conectar

Iniciar terraform e instala todo los complementos para correr el proyecto
```bash
make tf-init
```
Archivo donde se configuran los providers
```bash
providers.tf
```
Archivo donde se configuran los servicio
```bash
main.tf
```
Comando para verificar y listar todo lo que se va a servir
```bash
make tf-plan
```
Comando para aplicar/desplegar el plan
```bash
make tf-apply
```
Comando para destruir lo aplicado en el plan
```bash
make tf-destroy
```