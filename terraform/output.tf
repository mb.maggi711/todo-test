resource "local_file" "kubernetes_config" {
  content         = digitalocean_kubernetes_cluster.todonj.kube_config.0.raw_config
  filename        = local.kb
  file_permission = 0600
}
