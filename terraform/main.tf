resource "digitalocean_kubernetes_cluster" "todonj" {
  name   = "todonj"
  region = var.region
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = var.ver

  node_pool {
    name       = var.tg
    size       = var.size
    node_count = 1
    tags       = [var.tg]
  }
}
