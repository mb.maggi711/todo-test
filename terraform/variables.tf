variable "token" {
  type        = string
  description = "API token for DigitalOcean provider; pass using environmental variable $token"
}

variable "ver" {
  type    = string
  default = "1.30.1-do.0"
}

variable "region" {
  type    = string
  default = "nyc3"
}

variable "size" {
  type    = string
  default = "s-2vcpu-4gb"
}

variable "tg" {
  type    = string
  default = "todonj-nodes"
}

locals {
  root_dir = abspath(path.root)
  kb       = "${local.root_dir}/kubeconfig.yaml"
}
