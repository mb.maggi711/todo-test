# Configuración de Kubernetes

- Este repositorio contiene instrucciones, al igual que comandos para configurar y desplegar una aplicación en Kubernetes utilizando un archivo de configuración `../terraform/kubeconfig.yaml`.

## Pasos de Configuración

### Crear un Namespace
```sh
make -s k-ns
```
### Verificar la Creación del Namespace
```sh
make -s kca    
```
### Crear un Load Balancer (LB)
```sh
make -s kaf-lb 
```
### Verificar la Creación del Load Balancer
```sh
make -s kgs
```
### Crear un configmap
```sh
make -s kccm 
```
### Crear un Persistent Volume Claim (PVC)
```sh
make -s kaf-pvc
```
### Verificar la Creación del PVC
```sh
make -s kgpv
```
### Crear el Pod para MySQL
```sh
make -s kaf-my
```
### Verificar la Creación de MySQL
```sh
make -s kgp
```
### Crear el Pod para la Aplicación
```sh
make -s kaf-de
```
### Veridicar la creación del pod
```sh
make -s kgp
```
### Obtener la IP del Balanceador de Carga
```sh
make -s kgi
```
### Eliminar servicio usando archivo YAML

Reemplazar la palaba "archivo.yaml" por el nombre del YAML. Ejemplo make -s kdelf yaml=deployment.yaml

```sh
make -s kdelf yaml=archivo.yaml
```
### Eliminar pod

Reemplazar la palaba "name" por el nombre del pod.
```sh
make -s kdelp name=nombre
```